#!/usr/bin/env bash

#TODO: refactor to account for containerized ngspipeline
#      Don't need git user/token; we have public-facing repo that can be 
#      remote-deployed from local with gitlab username/password

. /opt/parallelcluster/cfnconfig

# Defined in post_install_args in config file
GIT_USER=${2}
GIT_TOKEN=${3}

GO_VERSION=1.11
SINGULARITY_RELEASE=3.1.1

echo "Executing post-install actions"

# EFS

sudo yum -y update
sudo yum -y install nfs-utils

# Docker
# For now, we'll use Docker, but soon we should transition to singularity

echo "Install docker..."
sudo yum install -y yum-util \
    device-mapper-persistent-data \
    lvm2
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
sudo yum -y install docker-ce docker-ce-cli containerd.io
echo "Starting docker"
sudo systemctl start docker
# Give user permission for docker
sudo groupadd docker
sudo gpasswd -a centos docker

# Just need to install minimal perl requirements for deployment and setup
echo "Install Perl Reqs..."
sudo yum -y install \
    perl-File-Find-Rule \
    perl-MIME-Lite

# Singularity
# Dependencies
sudo yum update -y && \
    sudo yum groupinstall -y 'Development Tools' && \
    sudo yum install -y \
    openssl-devel \
    libuuid-devel \
    libseccomp-devel \
    wget \
    squashfs-tools
# Install Go
export OS=linux ARCH=amd64 && \
    wget https://dl.google.com/go/go$GO_VERSION.$OS-$ARCH.tar.gz && \
    sudo tar -C /usr/local -xzvf go$GO_VERSION.$OS-$ARCH.tar.gz && \
    rm go$GO_VERSION.$OS-$ARCH.tar.gz
echo 'export GOPATH=${HOME}/go' >> ~/.bashrc && \
    echo 'export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin' >> ~/.bashrc && \
    source ~/.bashrc
go get -u github.com/golang/dep/cmd/dep
# # Install Singularity from release
mkdir -p $GOPATH/src/github.com/sylabs && \
    cd $GOPATH/src/github.com/sylabs && \
    wget https://github.com/sylabs/singularity/releases/download/v${SINGULARITY_RELEASE}/singularity-${SINGULARITY_RELEASE}.tar.gz && \
    tar -xzf singularity-${SINGULARITY_RELEASE}.tar.gz && \
    cd ./singularity && \
    ./mconfig

# NGS_pipeline and build image
# TODO: image should already be build for upload

# echo "Cloning NGS pipeline"
# cd /home/centos
# git clone http://${GIT_USER}:${GIT_TOKEN}@gitlab.com/lji-bioinformatics/ngs-pipeline.git
# # echo "Reading NGS pipeline container registry"
# # docker login registry.example.com -u gitlab+deploy-token-74401 -p RdNxHBKZZe9QB6uUUE4e
# 
# echo "To deploy, run: perl ngs-pipeline/deploy/deploy.pl --git_url git@gitlab.com:lji-bioinformatics/ngs-pipeline.git --project_url https://gitlab.com/lji-bioinformatics/ngs-pipeline"
# echo $GIT_USER
# echo $GIT_TOKEN
