```sh
source pcluster/bin/activate
pcluster create -c private/config -t torque-32 <name>
```

Wait for cluster to completely start before performing operations

Notes: 

- t2.micro compute instances should not be used
