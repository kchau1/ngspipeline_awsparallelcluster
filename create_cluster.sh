#!/usr/bin/env bash
# Convenience script to start a cluster
source pcluster/bin/activate
pcluster create -c private/config -t default test
