#!/usr/bin/env bash

aws efs create-file-system \
    --creation-token TestFileSystemBigClust \
    --tags Key=Name,Value=Test \
    --region us-west-1 \
    --profile default \
    > EFS_create.txt

aws efs create-mount-target \
    --file-system-id fs-40c94b59 \
    --subnet-id subnet-c8c5c593 \
    --security-group sg-079eafd7400c3ed4e \
    --region us-west-1 \
    --profile default

